@echo off

:: This batch file downloads an update for validate-harvest.py from GitLab
:: Last Updated: May 20, 2019

title Update-Harvest-Script

echo Updating Harvest Script...
echo.
echo.

:: Requires Git-Bash and Later Python3
echo This updater REQUIRES git!
echo The '.py' file REQUIRES Python3

:: Checking for Internet

echo Checking for Internet connection...
ping www.google.nl -n 1 -w 1000

if %errorlevel%==1 (
:: If connection fails...
echo No Internet connection...
echo Cannot update without Internet...
echo Exiting the updater...
pause
exit
)

:: If connection is successful...
echo Internet connection appears to be working...

:: Set Variables and Delete Current Files

set "URL=https://gitlab.com/jcarpinelli/validate-harvest.git/"

echo Deleting current files in order to update...
if exist .git rmdir /s /q .git

if exist validate-harvest rmdir /s /q validate-harvest

:: Update validate-harvest script

echo Adding validate-harvest repo
git clone %URL%
echo.

echo This script does not check for success...
echo If there are no errors, validate-harvest.py should be updated...
echo Update Complete!
echo.
echo.

:: End Script
pause
